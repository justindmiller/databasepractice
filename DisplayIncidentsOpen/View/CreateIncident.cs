﻿using DisplayIncidentsOpen.Controller;
using IncidentsData;
using IncidentsData.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DisplayIncidentsOpen.View
{
    public partial class CreateIncident : Form
    {
        public CreateIncident()
        {
            InitializeComponent();
        }

        private List<Customer> customerList;
        private List<Product> productList;


        private void createButton_Click(object sender, EventArgs e)
        {
            Incident newIncident = new Incident();
            newIncident.CustomerID = comboCustomer.SelectedValue.ToString();
            newIncident.ProductCode = comboProduct.SelectedValue.ToString();
            newIncident.Title = textTitle.Text;
            newIncident.DateOpened = System.DateTime.Now;
            newIncident.Description = textDescription.Text;
            if (newIncident.Title == "")
            {
                MessageBox.Show("Title cannot be empty", "Input Error");
            } else if (newIncident.Description == "") {
                MessageBox.Show("Description cannot be empty", "Input Error");
            } else
            {
                IncidentController.CreateIncident(newIncident);
                MessageBox.Show("Incident " + newIncident.Title + " added!", "Incident Accepted");
                this.Close();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadComboBoxes()
        {
            try
            {
                customerList = IncidentController.GetCustomers();
                comboCustomer.DataSource = customerList;
                comboCustomer.DisplayMember = "CustomerName";
                comboCustomer.ValueMember = "CustomerID";

                productList = IncidentController.GetProducts();
                comboProduct.DataSource = productList;
                comboProduct.DisplayMember = "ProductName";
                comboProduct.ValueMember = "ProductCode";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        private void CreateIncident_Load(object sender, EventArgs e)
        {
            this.LoadComboBoxes();
        }
    }
}
