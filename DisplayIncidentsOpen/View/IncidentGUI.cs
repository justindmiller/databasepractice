﻿using DisplayIncidentsOpen.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DisplayIncidentsOpen
{
    public partial class IncidentGUI : Form
    {
        public IncidentGUI()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        IncidentDisplay incidentDisplay;
        private void displayOpenIncidentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (incidentDisplay == null)
            {
                incidentDisplay = new View.IncidentDisplay();
                incidentDisplay.MdiParent = this;
                incidentDisplay.FormClosed += new FormClosedEventHandler(incidentDisplay_FormClosed);
                incidentDisplay.Show();
            }
            else
                incidentDisplay.Activate();
        }

        private void incidentDisplay_FormClosed(object sender, FormClosedEventArgs e)
        {
            incidentDisplay = null;
        }


        CreateIncident incidentCreation;
        private void createIncidentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (incidentCreation == null)
            {
                incidentCreation = new View.CreateIncident();
                incidentCreation.MdiParent = this;
                incidentCreation.FormClosed += new FormClosedEventHandler(createIncident_FormClosed);
                incidentCreation.Show();
            }
            else
                incidentCreation.Activate();
        }

        private void createIncident_FormClosed(object sender, FormClosedEventArgs e)
        {
            incidentCreation = null;
        }
    }
}
