﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DisplayIncidentsOpen.Controller;
using IncidentsData;

namespace DisplayIncidentsOpen.View
{
    public partial class IncidentDisplay : Form
    {
        /// <summary>
        /// Diplays the open incident data to a form based on the input form the SQL data
        /// </summary>

        private IncidentController incidentController;

        public IncidentDisplay()
        {
            InitializeComponent();
            incidentController = new IncidentController();
        }

        /// <summary>
        /// Populates the Form with the appropriate data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IncidentDisplay_Load(object sender, EventArgs e)
        {
            List<Incident> incidentList;
            try
            {
                incidentList = IncidentController.GetOpenIncidents();
                if (incidentList.Count > 0)
                {
                    Incident incident;
                    for (int i = 0; i < incidentList.Count; i++)
                    {
                        incident = incidentList[i];
                        lvIncidents.Items.Add(incident.ProductCode.ToString());
                        lvIncidents.Items[i].SubItems.Add(incident.DateOpened.ToShortDateString());
                        lvIncidents.Items[i].SubItems.Add(incident.Customer.ToString());
                        lvIncidents.Items[i].SubItems.Add(incident.Technician.ToString());
                        lvIncidents.Items[i].SubItems.Add(incident.Title.ToString());
                    }
                }
                else
                {
                    MessageBox.Show("No open incidents.", "All incidents resolved at present.");
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().ToString());
             
                this.Close();
            }

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
