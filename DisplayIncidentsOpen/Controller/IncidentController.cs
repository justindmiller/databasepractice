﻿using IncidentsData;
using IncidentsData.DAL;
using IncidentsData.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DisplayIncidentsOpen.Controller
{
    /// <summary>
    /// Controller class that deals with the DAL to prevent coupling with the View and DAL
    /// </summary>
    class IncidentController
    {

        public static List<Incident> GetOpenIncidents()
        {
            return IncidentDAL.GetOpenIncidents();
        }

        public static List<Customer> GetCustomers()
        {
            return IncidentDAL.GetCustomers();
        }

        public static List<Product> GetProducts()
        {
            return IncidentDAL.GetProducts();
        }

        public static void CreateIncident(Incident incident)
        {
           IncidentDAL.createIncident(incident);
        }
    }
}
