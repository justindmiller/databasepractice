﻿using IncidentsData.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentsData.DAL
{
    /// <summary>
    /// This is the DAL class that deals with incidents
    /// </summary>
    public static class IncidentDAL
    {
        ///This method will retrive the open incidents from the database and populate them into Incident List
        public static List<Incident> GetOpenIncidents()
        {
            List<Incident> incidentList = new List<Incident>();
            SqlConnection connection = IncidentsDBConnection.GetConnection();
            string selectStatement =
                "SELECT i.ProductCode, i.DateOpened, c.Name as Customer, t.Name as Technician, i.Title " +
                "FROM Incidents i " +
                "LEFT JOIN Customers c " +
                "ON i.CustomerID = c.CustomerID " +
                "LEFT JOIN Technicians t " +
                "ON t.TechID = i.TechID " +
                "WHERE i.DateClosed IS NULL " +
                "ORDER BY i.DateOpened";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                while (reader.Read())
                {
                    Incident incident = new Incident();
                    incident.ProductCode = reader["ProductCode"].ToString();
                    incident.DateOpened = (DateTime)reader["DateOpened"];
                    incident.Customer = reader["Customer"].ToString();
                    incident.Technician = reader["Technician"].ToString();
                    incident.Title = reader["Title"].ToString();
                    incidentList.Add(incident);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }


            return incidentList;
        }
        ///This method will retrive the customers from the database and populate them into Customer List
        public static List<Customer> GetCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            SqlConnection connection = IncidentsDBConnection.GetConnection();
            string selectStatement =
                "SELECT CustomerID, Name " +
                "FROM Customers " +
                "ORDER BY Name";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                while (reader.Read())
                {
                    Customer customer = new Customer();
                    customer.CustomerName = reader["Name"].ToString();
                    customer.CustomerID = reader["CustomerID"].ToString();
                    customerList.Add(customer);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }


            return customerList;
        }

        ///This method will retrive the products from the database and populate them into Product List
        public static List<Product> GetProducts()
        {
            List<Product> productList = new List<Product>();
            SqlConnection connection = IncidentsDBConnection.GetConnection();
            string selectStatement =
                "SELECT ProductCode, Name " +
                "FROM Products " +
                "ORDER BY Name";
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = selectCommand.ExecuteReader();

                while (reader.Read())
                {
                    Product product = new Product();
                    product.ProductName = reader["Name"].ToString();
                    product.ProductCode = reader["ProductCode"].ToString();
                    productList.Add(product);
                }

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
                if (reader != null)
                    reader.Close();
            }


            return productList;
        }
    
        /// <summary>
        /// This method creates a new incident in the TechSupport database
        /// </summary>
        /// <param name="incident"></param>
        public static void createIncident(Incident incident)
        {
            SqlConnection connection = IncidentsDBConnection.GetConnection();
            string insertStatement = "Insert Incidents " +
                 "(CustomerID, ProductCode, DateOpened, Title, Description) " +
                 "VALUES (@CustomerID, @ProductCode, @DateOpened, @Title, @Description)";
            SqlCommand insertCommand = new SqlCommand(insertStatement, connection);
            insertCommand.Parameters.AddWithValue("@CustomerID", incident.CustomerID);
            insertCommand.Parameters.AddWithValue("@ProductCode", incident.ProductCode);
            insertCommand.Parameters.AddWithValue("@DateOpened", incident.DateOpened);
            insertCommand.Parameters.AddWithValue("@Title", incident.Title);
            insertCommand.Parameters.AddWithValue("@Description", incident.Description);
            try
            {
                connection.Open();
                insertCommand.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
