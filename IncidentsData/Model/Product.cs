﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentsData.Model
{
    public class Product
    {
        public string ProductName { get; set; }

        public string ProductCode { get; set; }
    }
}
