﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentsData
{
    public class Incident
    {

        /// <summary>
        /// Set up basic getters and setters for variables that will be stored in an Incident
        /// </summary>

        public string ProductCode { get; set; }

        public DateTime DateOpened { get; set; }

        public string Customer { get; set; }

        public string Technician { get; set; }

        public string Title { get; set; }

        public string CustomerID { get; set; }

        public string Description { get; set; }
    }
}
