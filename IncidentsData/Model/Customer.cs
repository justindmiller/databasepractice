﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IncidentsData.Model
{
    public class Customer
    {
        public string CustomerName { get; set; }

        public string CustomerID { get; set; }
    }
}
